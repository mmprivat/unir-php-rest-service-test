UNIR
====

a universal infrared remote (learning project)

### What is this repository for? ###

this is a partial (learning) project which I will combine with some other 
(arduino based)  parts into a learning infrared remote with a web interface. 
It will be able to read / store its configuration from / to this RESTful 
WebService.

### How do I get set up? ###

 1. Store this file in your docroot/rest/ on an Apache / Nginx with PHP support
 2. Configure your server like this (for nginx):
 	location /rest/ {
 		# First attempt to serve request as file, then
 		# as directory, then fall back to displaying a 404.
 		try_files $uri $uri/ /rest/index.php?q=$args;
 		# Uncomment to enable naxsi on this location
 		# include /etc/nginx/naxsi.rules
 	}
 3. Test with a REST Client (like the Firefox Plugin RESTClient)

### Testing ###
Send a post request like:

		POST  http://yourserver.com/rest/datastore/uniir.cfg HTTP/1.1
		Host: yourserver.com
		User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:32.0) Gecko/20100101 Firefox/32.0
		Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
		Accept-Language: null`
		Accept-Encoding: gzip, deflate
		Content-Length: 180
		Content-Type: text/plain; charset=UTF-8
		Cookie: 7b4f80586a1f46c05a683c38ff90b54d=tqmLONtiTwBZUJwk.qkeOO4IwWcFFgEGWqhkH; 006a5871a96813a47a44079c36406a28=46c793cbea4676b19b0bf67aab5d6b18
		Connection: keep-alive
		Pragma: no-cache
		Cache-Control: no-cache
		
		[
			{"name":"Button1", "type":"single", "value":"#FFFFF"},
			{"name":"Button2", "type":"single", "value":"#FFFF0"},
			{"name":"Button2", "type":"multiple", "values":["#FFFFF", "#ASDF"]}
		]


Send a get request like:

		GET  http://yourserver.com/rest/datastore/uniir.cfg HTTP/1.1
		Host: yourserver.com
		User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:32.0) Gecko/20100101 Firefox/32.0
		Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
		Accept-Language: null
		Accept-Encoding: gzip, deflate
		Cookie: 7b4f80586a1f46c05a683c38ff90b54d=tqmLONtiTwBZUJwk.qkeOO4IwWcFFgEGWqhkH; 006a5871a96813a47a44079c36406a28=46c793cbea4676b19b0bf67aab5d6b18
		Connection: keep-alive